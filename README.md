# ME TOOLS DEMO
### This is demo application for [me-team\me-tools](https://gitlab.com/s1ro/me-tools) package

### Deployment

Docker:\
in .docker folder:
+ build```docker-compose build --no-cache```

in application folder:
+ up: ```docker-compose -f .docker/docker-compose.yml up -d```
+ down: ```docker-compose -f .docker/docker-compose.uml down```

in php-fpm container:
```
cat .env.sample > .env
composer install
php bin/console d:m:m
```

in node.js container:
```
npm install
npm run dev
```
