<?php

declare(strict_types=1);

namespace App\Domain\User;

use App\Entity\User;
use Symfony\Component\HttpFoundation\RequestStack;

class DefaultUserService implements UserServiceInterface
{
    public function __construct(private RequestStack $requestStack, private UserRepositoryInterface $userRepository) {}

    public function loadUserByToken(): ?User
    {
        $request = $this->requestStack->getCurrentRequest();
        if (!$request) {
            return null;
        }
        $token = $request->headers->get('X-AUTH-TOKEN');
        if (!$token) {
            return null;
        }
        return $this->userRepository->findByToken($token);
    }
}