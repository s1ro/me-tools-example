<?php

declare(strict_types=1);

namespace App\Domain\User;

use App\Entity\User;

interface UserRepositoryInterface
{
    public function findByToken(string $token): ?User;
}