<?php

declare(strict_types=1);

namespace App\Domain\User;

use App\Entity\User;

interface UserServiceInterface
{
    public function loadUserByToken(): ?User;
}