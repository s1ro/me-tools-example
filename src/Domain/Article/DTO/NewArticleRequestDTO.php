<?php

declare(strict_types=1);

namespace App\Domain\Article\DTO;

use App\Domain\Article\Infrastructure\Validation\ArticleText\ArticleTextConstraint;
use App\Domain\Article\Infrastructure\Validation\ArticleTitle\ArticleTitleConstraint;
use MeTools\Http\Request\RequestDTOInterface;
use Symfony\Component\HttpFoundation\Request;

class NewArticleRequestDTO implements RequestDTOInterface
{
    private Request $originalRequest;

    #[ArticleTitleConstraint]
    private string $title;
    #[ArticleTextConstraint]
    private string $text;

    public function __construct(Request $request)
    {
        $this->originalRequest = $request;
        $this->title = $request->request->get('title', '');
        $this->text = $request->request->get('text', '');
    }

    public function getOriginalRequest(): Request
    {
        return $this->originalRequest;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getText(): string
    {
        return $this->text;
    }
}