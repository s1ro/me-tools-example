<?php

declare(strict_types=1);

namespace App\Domain\Article;

use App\Domain\Article\DTO\NewArticleRequestDTO;
use App\Domain\Article\DTO\UpdateArticleRequestDTO;
use App\Entity\Article;
use App\Entity\User;

interface ArticleServiceInterface
{
    public function addArticle(NewArticleRequestDTO $articleRequestDTO, User $user): Article;

    public function updateArticle(Article $article, UpdateArticleRequestDTO $updateArticleRequestDTO): Article;

    public function isOwner(Article $article, User $user): bool;
}