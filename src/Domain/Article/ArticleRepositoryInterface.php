<?php

declare(strict_types=1);

namespace App\Domain\Article;

use App\Entity\Article;
use App\Entity\User;
use MeTools\Core\Exception\InfrastructureException;

interface ArticleRepositoryInterface
{
    /**
     * @throws InfrastructureException
     */
    public function save(Article $article): void;

    /**
     * @return ArticleCollection|Article[]
     */
    public function getUserArticles(User $user): ArticleCollection;

    /**
     * @return ArticleCollection|Article[]
     */
    public function getAll(): ArticleCollection;
}