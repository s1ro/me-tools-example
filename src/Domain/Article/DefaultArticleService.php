<?php

declare(strict_types=1);

namespace App\Domain\Article;

use App\Domain\Article\DTO\NewArticleRequestDTO;
use App\Domain\Article\DTO\UpdateArticleRequestDTO;
use App\Domain\Article\ValueObject\ArticleText;
use App\Domain\Article\ValueObject\ArticleTitle;
use App\Entity\Article;
use App\Entity\User;

class DefaultArticleService implements ArticleServiceInterface
{
    public function __construct(private ArticleRepositoryInterface $articleRepository) {}

    public function addArticle(NewArticleRequestDTO $articleRequestDTO, User $user): Article
    {
        $article = new Article();
        $article
            ->setCreatedBy($user)
            ->setTitle(ArticleTitle::make($articleRequestDTO->getTitle()))
            ->setArticleText(ArticleText::make($articleRequestDTO->getText()));
        $this->articleRepository->save($article);
        return $article;
    }

    public function updateArticle(Article $article, UpdateArticleRequestDTO $updateArticleRequestDTO): Article
    {
        if ($updateArticleRequestDTO->getTitle() !== null) {
            $article->setTitle(ArticleTitle::make($updateArticleRequestDTO->getTitle()));
        }
        if ($updateArticleRequestDTO->getText() !== null) {
            $article->setArticleText(ArticleText::make($updateArticleRequestDTO->getText()));
        }
        $this->articleRepository->save($article);
        return $article;
    }

    public function isOwner(Article $article, User $user): bool
    {
        return $article->getCreatedBy() === $user;
    }
}