<?php

declare(strict_types=1);

namespace App\Domain\Article\ValueObject\Exception;

use MeTools\Core\Error\ErrorCode;
use MeTools\Core\Exception\ValidationException;

class InvalidArticleTitleException extends ValidationException
{
    public static function emptyTitle(): static
    {
        return new static('Article title can\'t be empty.', ErrorCode::BAD_REQUEST);
    }
}