<?php

declare(strict_types=1);

namespace App\Domain\Article\ValueObject\Exception;

use MeTools\Core\Error\ErrorCode;
use MeTools\Core\Exception\ValidationException;

class InvalidArticleTextException extends ValidationException
{
    public static function emptyArticle(): static
    {
        return new static('Article text can\'t be empty.', ErrorCode::BAD_REQUEST);
    }
}