<?php

declare(strict_types=1);

namespace App\Domain\Article\ValueObject;

use App\Domain\Article\ValueObject\Exception\InvalidArticleTitleException;
use MeTools\Core\ValueObject\ValueObject;

class ArticleTitle extends ValueObject
{
    private string $title;

    /**
     * @throws InvalidArticleTitleException
     */
    protected function __construct(string $title)
    {
        if ('' === $title) {
            throw InvalidArticleTitleException::emptyTitle();
        }
        $this->title = $title;
    }

    /**
     * @throws InvalidArticleTitleException
     */
    public static function make(string $title): static
    {
        return new static($title);
    }

    public function get(): string
    {
        return $this->title;
    }
}