<?php

declare(strict_types=1);

namespace App\Domain\Article\ValueObject;

use App\Domain\Article\ValueObject\Exception\InvalidArticleTextException;
use MeTools\Core\ValueObject\ValueObject;

class ArticleText extends ValueObject
{
    private string $text;

    /**
     * @throws InvalidArticleTextException
     */
    public function __construct(string $text)
    {
        if ('' === $text) {
            throw InvalidArticleTextException::emptyArticle();
        }
        $this->text = $text;
    }

    public static function make(string $text): static
    {
        return new static($text);
    }

    public function get(): string
    {
        return $this->text;
    }
}