<?php

declare(strict_types=1);

namespace App\Domain\Article;

use App\Entity\Article;
use MeTools\Core\Collection\Collection;


class ArticleCollection extends Collection
{
    protected ?string $allowedType = Article::class;
}