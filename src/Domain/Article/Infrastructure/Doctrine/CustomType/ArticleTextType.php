<?php

declare(strict_types=1);

namespace App\Domain\Article\Infrastructure\Doctrine\CustomType;

use App\Domain\Article\ValueObject\ArticleText;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;

class ArticleTextType extends Type
{
    private const NAME = 'article_text';

    public function getSQLDeclaration(array $column, AbstractPlatform $platform): string
    {
        return 'LONGTEXT';
    }

    public function getName(): string
    {
        return self::NAME;
    }

    public function convertToPHPValue($value, AbstractPlatform $platform): ArticleText
    {
        return ArticleText::make($value);
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform): string
    {
        /** @var ArticleText $value */
        return $value->get();
    }
}