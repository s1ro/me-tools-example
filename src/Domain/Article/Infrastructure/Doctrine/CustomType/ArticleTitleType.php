<?php

declare(strict_types=1);

namespace App\Domain\Article\Infrastructure\Doctrine\CustomType;

use App\Domain\Article\ValueObject\ArticleTitle;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;

class ArticleTitleType extends Type
{
    private const NAME = 'article_title';

    public function getSQLDeclaration(array $column, AbstractPlatform $platform): string
    {
        return 'VARCHAR(100)';
    }

    public function getName(): string
    {
        return self::NAME;
    }

    public function convertToPHPValue($value, AbstractPlatform $platform): ArticleTitle
    {
        return ArticleTitle::make($value);
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform): string
    {
        /** @var ArticleTitle $value */
        return $value->get();
    }
}