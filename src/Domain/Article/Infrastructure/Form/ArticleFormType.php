<?php

declare(strict_types=1);

namespace App\Domain\Article\Infrastructure\Form;

use App\Domain\Article\ValueObject\ArticleText;
use App\Domain\Article\ValueObject\ArticleTitle;
use App\Entity\Article;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ArticleFormType extends AbstractType
{
    /**
     * @inheritDoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('title', null, [
                'required' => true,
                'setter' => static function (Article &$article, ?string $value) {
                    $article->setTitle(ArticleTitle::make($value));
                }
            ])
            ->add('articleText', TextareaType::class, [
                'required' => true,
                'setter' => static function (Article &$article, ?string $value) {
                    $article->setArticleText(ArticleText::make($value));
                }
            ]);
    }

    /**
     * @inheritDoc
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(['data_class' => Article::class]);
    }
}