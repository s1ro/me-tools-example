<?php

declare(strict_types=1);

namespace App\Domain\Article\Infrastructure\Validation\ArticleTitle;

use App\Domain\Article\ValueObject\ArticleTitle;
use App\Domain\Article\ValueObject\Exception\InvalidArticleTitleException;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class ArticleTitleConstraintValidator extends ConstraintValidator
{
    public function validate(mixed $value, Constraint $constraint): void
    {
        if (!$constraint instanceof ArticleTitleConstraint) {
            throw new UnexpectedTypeException($constraint, ArticleTitleConstraint::class);
        }
        if (is_null($value) && $constraint->nullable) {
            return;
        }
        try {
            ArticleTitle::make((string)$value);
        } catch (InvalidArticleTitleException $e) {
            $this->context->buildViolation($e->getMessage())
                ->setCode((string)$e->getCode())
                ->addViolation();
        }
    }
}