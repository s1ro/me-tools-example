<?php

declare(strict_types=1);

namespace App\Domain\Article\Infrastructure\Validation\ArticleTitle;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
#[\Attribute]
class ArticleTitleConstraint extends Constraint
{
    public bool $nullable;

    public function __construct(bool $nullable = false, mixed $options = null, array $groups = null, mixed $payload = null)
    {
        $this->nullable = $nullable;
        parent::__construct($options, $groups, $payload);
    }
}