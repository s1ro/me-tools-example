<?php

declare(strict_types=1);

namespace App\Domain\Article\Infrastructure\Validation\ArticleText;

use App\Domain\Article\ValueObject\ArticleText;
use App\Domain\Article\ValueObject\Exception\InvalidArticleTextException;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class ArticleTextConstraintValidator extends ConstraintValidator
{
    public function validate(mixed $value, Constraint $constraint): void
    {
        if (!$constraint instanceof ArticleTextConstraint) {
            throw new UnexpectedTypeException($constraint, ArticleTextConstraint::class);
        }
        if (is_null($value) && $constraint->nullable) {
            return;
        }
        try {
            ArticleText::make((string)$value);
        } catch (InvalidArticleTextException $e) {
            $this->context->buildViolation($e->getMessage())
                ->setCode((string)$e->getCode())
                ->addViolation();
        }
    }
}