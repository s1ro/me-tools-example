<?php

declare(strict_types=1);

namespace App\Repository;

use App\Domain\Article\ArticleCollection;
use App\Domain\Article\ArticleRepositoryInterface;
use App\Entity\Article;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;
use MeTools\Core\Exception\InfrastructureException;

/**
 * @method Article|null find($id, $lockMode = null, $lockVersion = null)
 * @method Article|null findOneBy(array $criteria, array $orderBy = null)
 * @method Article[]    findAll()
 * @method Article[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ArticleRepository extends ServiceEntityRepository implements ArticleRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Article::class);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add(Article $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function remove(Article $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @throws InfrastructureException
     */
    public function save(Article $article): void
    {
        try {
            $this->add($article, true);
        } catch (\Throwable $t) {
            throw InfrastructureException::fromUnexpectedThrowable($t);
        }
    }

    public function getUserArticles(User $user): ArticleCollection
    {
        return new ArticleCollection($this->findBy(['createdBy' => $user]));
    }

    public function getAll(): ArticleCollection
    {
        return new ArticleCollection($this->findAll());
    }
}
