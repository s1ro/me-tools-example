<?php

declare(strict_types=1);

namespace App\Entity;

use App\Domain\Article\ValueObject\ArticleText;
use App\Domain\Article\ValueObject\ArticleTitle;
use App\Repository\ArticleRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ArticleRepository::class)]
class Article implements \JsonSerializable
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private ?int $id;

    #[ORM\Column(type: 'article_title')]
    private ?ArticleTitle $title;

    #[ORM\Column(type: 'article_text')]
    private ?ArticleText $articleText;

    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'articles')]
    private ?User $createdBy;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?ArticleTitle
    {
        return $this->title;
    }

    public function setTitle(ArticleTitle $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getArticleText(): ?ArticleText
    {
        return $this->articleText;
    }

    public function setArticleText(ArticleText $articleText): self
    {
        $this->articleText = $articleText;

        return $this;
    }

    public function getCreatedBy(): ?User
    {
        return $this->createdBy;
    }

    public function setCreatedBy(?User $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function jsonSerialize(): array
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'articleText' => $this->articleText,
            'createdBy' => $this->createdBy,
        ];
    }
}
