<?php

declare(strict_types=1);

namespace App\Controller\Api;

use App\Domain\Article\ArticleRepositoryInterface;
use App\Domain\Article\ArticleServiceInterface;
use App\Domain\Article\DTO\NewArticleRequestDTO;
use App\Domain\Article\DTO\UpdateArticleRequestDTO;
use App\Domain\User\UserServiceInterface;
use App\Entity\Article;
use MeTools\Core\Error\ErrorCode;
use MeTools\Core\Error\ErrorCollection;
use MeTools\Core\Exception\ApiException;
use MeTools\Core\Exception\NotFoundException;
use MeTools\Http\Controller\ApiController;
use MeTools\Http\Request\RequestValidator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

#[Route('/api/article')]
class ArticleController extends ApiController
{
    #[Route('/', name: 'api_article_all', methods: ['GET'])]
    public function getAllArticles(
        ArticleRepositoryInterface $articleRepository,
        SerializerInterface $serializer
    ): JsonResponse
    {
        return $this->json($articleRepository->getAll());
    }

    #[Route('/{article<\d+>}', name: 'api_article', methods: ['GET'])]
    public function getArticle(?Article $article = null): JsonResponse
    {
        if (!$article) {
            throw ApiException::fromThrowable(new NotFoundException('Article not found.'));
        }
        return $this->json($article);
    }

    #[Route('/new', 'api_article_new', methods: ['POST'])]
    public function addArticle(
        NewArticleRequestDTO $articleRequestDTO,
        ArticleServiceInterface $articleService,
        UserServiceInterface $userService,
        RequestValidator $requestValidator
    ): JsonResponse
    {
        $user = $userService->loadUserByToken();
        if (!$user) {
            throw ApiException::fromThrowable(new \Exception('Authorization Error', ErrorCode::BAD_REQUEST));
        }
        $this->validateRequest($articleRequestDTO);
        try {
            return $this->json($articleService->addArticle($articleRequestDTO, $user));
        } catch (\Throwable $t) {
            throw ApiException::fromThrowable($t);
        }
    }

    #[Route('/owned', 'api_article_owned', methods: ['GET'])]
    public function getOwnedArticle(
        UserServiceInterface $userService,
        ArticleRepositoryInterface $articleRepository
    ): JsonResponse
    {
        $user = $userService->loadUserByToken();
        if (!$user) {
            throw ApiException::fromThrowable(new \Exception('Authorization Error', ErrorCode::BAD_REQUEST));
        }
        return $this->json($articleRepository->getUserArticles($user));
    }

    #[Route('/{article}/edit', name: 'api_article_edit', methods: ['POST'])]
    public function edit(
        Article $article,
        UpdateArticleRequestDTO $articleRequestDTO,
        UserServiceInterface $userService,
        ArticleServiceInterface $articleService,
        RequestValidator $requestValidator
    ): JsonResponse
    {
        $user = $userService->loadUserByToken();
        if (!$user || !$articleService->isOwner($article, $user)) {
            throw ApiException::fromThrowable(new \Exception('Authorization Error', ErrorCode::BAD_REQUEST));
        }
        $this->validateRequest($articleRequestDTO);
        return $this->json($articleService->updateArticle($article, $articleRequestDTO));
    }
}