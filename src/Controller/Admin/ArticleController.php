<?php

declare(strict_types=1);

namespace App\Controller\Admin;

use App\Domain\Article\ArticleRepositoryInterface;
use App\Domain\Article\Infrastructure\Form\ArticleFormType;
use App\Entity\Article;
use App\Entity\User;
use App\Security\Voter\ArticleVoter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/admin/article')]
#[IsGranted('ROLE_USER')]
class ArticleController extends AbstractController
{
    #[Route('/new', name: 'admin_article_new', methods: ['GET', 'POST'])]
    public function new(Request $request, ArticleRepositoryInterface $articleRepository): Response
    {
        /** @var User $user */
        $user = $this->getUser();
        $article = new Article();
        $article->setCreatedBy($user);
        $form = $this->createForm(ArticleFormType::class, $article);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $articleRepository->save($article);
            return $this->redirectToRoute('index');
        }
        return $this->render('admin/article/new.html.twig', [
            'article' => $article,
            'form' => $form->createView(),
        ]);
    }

    #[Route('/', name: 'admin_article_index', methods: ['GET'])]
    public function index(ArticleRepositoryInterface $articleRepository): Response
    {
        /** @var User $user */
        $user = $this->getUser();
        return $this->render('admin/article/index.html.twig', [
            'articles' => $articleRepository->getUserArticles($user),
        ]);
    }

    #[Route('/{article}/edit', name: 'admin_article_edit', methods: ['GET', 'POST'])]
    public function edit(Article $article, Request $request, ArticleRepositoryInterface $articleRepository): Response
    {
        $this->denyAccessUnlessGranted(ArticleVoter::EDIT, $article);
        $form = $this->createForm(ArticleFormType::class, $article);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $articleRepository->save($article);
            return $this->redirectToRoute('admin_article_index');
        }

        return $this->render('admin/article/edit.html.twig', [
            'article' => $article,
            'form' => $form->createView(),
        ]);
    }
}