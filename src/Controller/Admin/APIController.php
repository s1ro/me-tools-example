<?php

declare(strict_types=1);

namespace App\Controller\Admin;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/admin/api')]
#[IsGranted('ROLE_USER')]
class APIController extends AbstractController
{
    #[Route('/', name: 'admin_api_index', methods: ['GET'])]
    public function index(): Response
    {
        return $this->render('admin/api/index.html.twig');
    }
}