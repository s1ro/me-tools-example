<?php

declare(strict_types=1);

namespace App\Controller;

use App\Domain\Article\ArticleRepositoryInterface;
use App\Entity\Article;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends AbstractController
{
    #[Route('/', name: 'index', methods: ['GET'])]
    public function index(ArticleRepositoryInterface $articleRepository): Response
    {
        return $this->render('index.html.twig', [
            'articles' => $articleRepository->getAll(),
        ]);
    }

    #[Route('/article/{article}', name: 'article', methods: ['GET'])]
    public function article(Article $article): Response
    {
        return $this->render('article.html.twig', [
            'article' => $article,
        ]);
    }
}