<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\User;
use App\Infrastructure\Form\RegistrationFormType;
use Doctrine\ORM\EntityManagerInterface;
use MeTools\Core\ValueObject\Token;
use Ramsey\Uuid\Uuid;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\AuthenticationEvents;
use Symfony\Component\Security\Core\Event\AuthenticationEvent;

class RegistrationController extends AbstractController
{
    #[Route('/register', name: 'app_register')]
    public function register(
        Request $request, UserPasswordHasherInterface $userPasswordHasher,
        EntityManagerInterface $entityManager, EventDispatcherInterface $dispatcher,
        TokenStorageInterface $tokenStorage, SessionInterface $session
    ): Response
    {
        $user = new User();
        $form = $this->createForm(RegistrationFormType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // encode the plain password
            $user->setPassword(
            $userPasswordHasher->hashPassword(
                    $user,
                    $form->get('plainPassword')->getData()
                )
            );

            $user->setToken(Token::make((string)Uuid::uuid4()));

            $entityManager->persist($user);
            $entityManager->flush();
            // do anything else you need here, like send an email

            $token = new UsernamePasswordToken($user,  'main', $user->getRoles());
            $tokenStorage->setToken($token);
            $session->set('_security_main', serialize($token));
            $event = new AuthenticationEvent($token);
            $dispatcher->dispatch($event, AuthenticationEvents::AUTHENTICATION_SUCCESS);

            return $this->redirectToRoute('admin_article_new');
        }

        return $this->render('registration/register.html.twig', [
            'registrationForm' => $form->createView(),
        ]);
    }
}
